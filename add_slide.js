var c = 0;
console.log(c);
(function ($) {
  $('#add-slide-button').click(function(e) {
    e.preventDefault();

    var sheep = $('#carousel-editor-slide-'+c);
    c++;
    var klone = $(sheep).clone().attr('id', 'carousel-editor-slide-'+c);
    $(klone).insertAfter(sheep);
    $('#carousel-editor-slide-'+c+' :input').each(function(i, obj){
      var field_id = $(obj).attr('id');
      if (field_id.length > 0) {
        var new_id = field_id+'-'+c;
        $('#carousel-editor-slide-'+c).find('#'+field_id).attr('id', new_id);
      }
      var field_name = $(obj).attr('name');
      var regexPattern = /files\[(.*)\]/g;
      var match = regexPattern.exec(field_name);
      if (match) {
        if (!isNaN(match[1].substring(match[1].length-1))) {
          var iteration = match[1].substring(match[1].length-1);
          var new_name = field_name.replace(iteration, c);
          console.log('its numeric...');
        } else {
          console.log('not numeric...');
          var new_name = 'files[' + match[1] + '_' + c + ']';
        }
      } else {
        var new_name = field_name + '_' + c;
      }
      console.log('OLD NAME: ' + field_name);
      console.log('NEW NAME: ' + new_name);
      console.log($('#carousel-editor-slide-'+c).find('input[name^=\''+field_name+'\']').attr('name'));
      $('#carousel-editor-slide-'+c).find('input[name^=\''+field_name+'\']').attr('name', new_name);
      console.log($('#carousel-editor-slide-'+c).find('input[name^=\''+new_name+'\']').attr('name'));
    });

  });
})(jQuery);
