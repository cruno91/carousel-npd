/**
 * @file
 * Javascript for Field Example.
 */

/**
 * Provides a means for inputting and retrieving carousel data.
 */
var c = 0;
(function ($) {
  var nid;
  Drupal.carousel_npd_data_form = Drupal.carousel_npd_data_form || {};
  Drupal.behaviors.carousel_npd_data_form = {
    attach: function(context, settings) {
      var nid = Drupal.settings.carousel_npd_data_form.nid;
      var field_name = Drupal.settings.carousel_npd_data_form.field_name;
      var form_location = 'carousel/form';
      var node_carousel_form_location = 'carousel/form/' + nid + '/' + field_name;

      init();

      /**
       * Run some stuff before stuff happens.
       */
      function init() {
        var nodeResponse = retrieveNodeCarousel();
        var carousel_data = nodeResponse.response;
        if (carousel_data == '{carousel:[]}' || " " || "") {
          // carousel data for node is empty
        } else {
          // carousel data for node exists
        }
      }

      /**
       * Returns the carousel JSON data for a node.
       * @return {[JSON]} node carousel value
       */
      function retrieveNodeCarousel() {
        return $.ajax({
          async: false,
          type: "POST",
          url: Drupal.settings.basePath + node_carousel_form_location,
        });
      }

    }
  }
})(jQuery);


/*
// Add Slide button info (since I can't put inline comments in the onclick of
   of the form element definition in carousel_npd_form_alter in
   carousel_npd.module).
// Stop button from submitting the whole page
event.preventDefault();
// Get the object for the slide form
var sheep = (jQuery)(\'#carousel-editor-slide-\'+c);
// Increment when the button is pushed so we can rename things as they're added
c++
// Create a clone of the slide form and change the ID
var klone = (jQuery)(sheep).clone().attr(\'id\', \'carousel-editor-slide-\'+c);
console.log(klone);
// Place the clone right after the original
(jQuery)(klone).insertAfter(sheep);
// Iterate through all input elements in the new clone
(jQuery)(\'#carousel-editor-slide-\'+c+\' :input\').each(function(i, obj){
  console.log(obj);
  // Get the ID of the current input element
  var field_id = (jQuery)(obj).attr(\'id\');
  if (field_id.length > 0) {
    // Create the new ID for the input element
    var new_id = field_id+\'-\'+c;
    // Update the new input element with a new ID
    (jQuery)(\'#carousel-editor-slide-\'+c).find(\'#\'+field_id).attr(\'id\', new_id);
  }
  // Get the name attribute of the current input element
  var field_name = (jQuery)(obj).attr(\'name\');
  // Create a new name attribute for the input element
  var new_name = field_name+\'_\'+c;
  console.log(new_name);
  // Update the new input element with a new name attribute
  (jQuery)(\'#carousel-editor-slide-\'+c+\' :input[name^=\'+field_name+\']\').attr(\'name\', new_name);
  console.log((jQuery)(\'#carousel-editor-slide-\'+c+\' :input[name^=\'+new_name+\']\'));
});
 */
